// import axios from 'axios';

import axios from "axios";

const state = {
    todos: []
};

const getters = {
    allTodos: (state) => state.todos
};

const actions = {
    async fetchTodos({ commit }) {
        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/todos?_limit=5')
            commit('setTodos', response.data);
        } catch (error) {
            console.log(error)
        }
    },
    async addTodo({ commit }, title) {
        try {
            const requestData = { title, completed: false}
            const response = await axios.post('https://jsonplaceholder.typicode.com/todos?_limit=5', requestData)
            commit('newTodo', response.data);
        } catch (error) {
            console.log(error)
        }
    },
    async delTodo({ commit }, id) {
        try {
            await axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
            commit('delTodo', id);
        } catch (error) {
            console.log(error)
            console.log(id)
        }
    },
    async toggleComplete({ commit }, todo) {
        try {
            const requestData = { todo }
            await axios.put(`https://jsonplaceholder.typicode.com/todos/${todo.id}`, requestData)
            commit('togComplete', todo)
        } catch (error) {
            console.log(error)
        }
    }
};

const mutations = {
    setTodos: (state, todos) => (state.todos = todos),
    newTodo: (state, todo) => (state.todos.unshift(todo)),
    delTodo: (state, id) => (state.todos = state.todos.filter(todo => todo.id !== id)),
    togComplete: (state, todo) => {
        const index = state.todos.indexOf(todo)
        if (index !== -1) {
            state.todos[index] = todo;
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}