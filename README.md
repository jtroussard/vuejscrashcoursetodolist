# Credit
- VueJsCrashCourseToDoList
  - Vue JS Crash Course (2019)
  -  Traversy Media
  - https://youtu.be/Wy9q22isx3U

# Disclaimer
This repository was created to help sync progress across machines and left public as a guide for others. All code included in this repo is either a direct copy or unoriginal derivative coming from a public/unpaid/free tutorial. Do not hesitate to reach out for take down request, all requests from original authors will be honored. All intentions with this repository were made in good faith, and for educational reasons.

# Project README
# vue_todo_list

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
